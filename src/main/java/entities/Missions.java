package entities;

import java.util.*;

import javax.persistence.*;

@Entity
@Table(name="missions")
public class Missions {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;

	@Column(name="name", nullable=false)
	private String name;

	@Column(name="level", nullable=false)
	private int level;

	@Column(name="bounty", nullable=false)
	private int bounty;

	@ManyToMany(cascade=CascadeType.ALL)
	private List<Crews> crews = new ArrayList<Crews>();
	
	@ManyToOne(cascade=CascadeType.ALL)
	private Planets questPlanet;

	@ManyToOne(cascade=CascadeType.ALL)
	private Planets locationPlanet;


	@Version
	private int version;

	public Missions(String name, int level, int bounty, Planets questPlanet, Planets locationPlanet) {
		this.name = name;
		this.level = level;
		this.bounty = bounty;
		this.questPlanet = questPlanet;
		this.locationPlanet = locationPlanet;
	}

	public Missions() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getBounty() {
		return bounty;
	}

	public void setBounty(int bounty) {
		this.bounty = bounty;
	}

	public Planets getQuestPlanet() {
		return questPlanet;
	}

	public void setQuestPlanet(Planets questPlanet) {
		this.questPlanet = questPlanet;
	}

	public Planets getLocationPlanet() {
		return locationPlanet;
	}

	public void setLocationPlanet(Planets locationPlanet) {
		this.locationPlanet = locationPlanet;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String toString() {
		return "Missions [id=" + id + ", name=" + name + ", level=" + level + ", bounty=" + bounty + ", questPlanet="
				+ questPlanet + ", locationPlanet=" + locationPlanet + "]";
	}
	
}
