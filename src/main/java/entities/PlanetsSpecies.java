package entities;

import java.util.*;

import javax.persistence.*;

@Entity
@Table(name="planets_species")
public class PlanetsSpecies {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;

	@Column(name="name", nullable=false)
	private String name;

	@ManyToMany(cascade=CascadeType.ALL)
	private List<Planets> planets = new ArrayList<Planets>();

	@Version
	private int version;

	public PlanetsSpecies() {
	}

	public PlanetsSpecies(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	public List<Planets> getPlanets() {
		return planets;
	}

	public void setPlanets(List<Planets> planets) {
		this.planets = planets;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	
	
	public void addPlanet(Planets p) {
		planets.add(p);
	}

	@Override
	public String toString() {
		return "Species [name=" + name + "]";
	}
}
