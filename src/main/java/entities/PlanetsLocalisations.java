package entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="planets_localisations")
public class PlanetsLocalisations {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;

	@Column(name="name", nullable=false)
	private String name;
	
	// @ManyToOne(cascade=CascadeType.ALL)
	// private Planets planet;

	@OneToMany(mappedBy="planetsLocalisations")
	private List<Planets> planetsLocalisations= new ArrayList<Planets>();
	
	@Version
	private int version;

	public PlanetsLocalisations(String name) {
		this.name = name;
	}
	
	public PlanetsLocalisations() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	/*
	public Planets getPlanet() {
		return planet;
	}

	public void setPlanet(Planets planet) {
		this.planet = planet;
	}
	*/

	public void addPlanet(Planets p) {
		planetsLocalisations.add(p);
	}
	
	public List<Planets> getPlanetsTypes() {
		return planetsLocalisations;
	}

	public void setPlanetsTypes(List<Planets> planetsTypes) {
		this.planetsLocalisations = planetsTypes;
	}

	public String toString() {
		return "PlanetsLocalisations [id=" + id + ", name=" + name + "]";
	}
	
}
