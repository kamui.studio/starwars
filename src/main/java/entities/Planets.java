package entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="planets")
public class Planets {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;

	@Column(name="name", nullable=false)
	private String name;

	@OneToMany(mappedBy="questPlanet")
	private List<Missions> questMissions = new ArrayList<Missions>();
	
	@OneToMany(mappedBy="locationPlanet")
	private List<Missions> locationMissions = new ArrayList<Missions>();

	@ManyToMany(mappedBy="planets")
	private List<PlanetsLandscapes> landscapes = new ArrayList<PlanetsLandscapes>();
	
	@ManyToMany(mappedBy="planets")
	private List<PlanetsSpecies> species = new ArrayList<PlanetsSpecies>();

	@ManyToOne(cascade=CascadeType.ALL)
	private PlanetsTypes planetsTypes;
	
	@ManyToOne(cascade=CascadeType.ALL)
	private PlanetsLocalisations planetsLocalisations;
	
	@Version
	private int version;




	public Planets(String name) {
		this.name = name;
	}

	public Planets() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	
	public List<Missions> getQuestMissions() {
		return questMissions;
	}

	public void setQuestMissions(List<Missions> questMissions) {
		this.questMissions = questMissions;
	}

	public List<Missions> getLocationMissions() {
		return locationMissions;
	}

	public void setLocationMissions(List<Missions> locationMissions) {
		this.locationMissions = locationMissions;
	}

	
	public void addQuestMission(Missions questMission) {
		questMissions.add(questMission);
	}
	
	public void addLocationMission(Missions locationMission) {
		questMissions.add(locationMission);
	}

	public void addLanscape(PlanetsLandscapes l) {
		landscapes.add(l);
	}

	public void addSpecies(PlanetsSpecies s) {
		species.add(s);
	}
	
	@Override
	public String toString() {
		return "Planets [name=" + name + "]";
	}
	
	
}
