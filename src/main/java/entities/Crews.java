package entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="crews")
public class Crews {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;

	@Column(name="name", nullable=false)
	private String name;
	
	@Column(name="bounty")
	private int bounty = 1000;
	
	@Column(name="balance")
	private int balance;
	
	@OneToOne(cascade=CascadeType.ALL)
	private Ships ship;
	
	@ManyToOne(cascade=CascadeType.ALL)
	private CrewsAffiliations crewAffiliation;
	
	@ManyToMany(mappedBy="crews")
	private List<Missions> missions = new ArrayList<Missions>();

	@Version
	private int version;

	public Crews(String name, int bounty, int balance, Ships ship) {
		this.name = name;
		this.bounty = bounty;
		this.balance = balance;
		this.ship = ship;
	}

	public Crews() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBounty() {
		return bounty;
	}

	public void setBounty(int bounty) {
		this.bounty = bounty;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public Ships getShip() {
		return ship;
	}

	public void setShip(Ships ship) {
		this.ship = ship;
	}

	public void addMission(Missions m) { 
		missions.add(m);
	}
	
	public List<Missions> getMissions() {
		return missions;
	}

	public void setMissions(List<Missions> missions) {
		this.missions = missions;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public CrewsAffiliations getCrewAffiliation() {
		return crewAffiliation;
	}

	public void setCrewAffiliation(CrewsAffiliations crewAffiliation) {
		this.crewAffiliation = crewAffiliation;
	}

	public String toString() {
		return "Crews [id=" + id + ", name=" + name + ", bounty=" + bounty + ", balance=" + balance
				+ ", ship=" + ship + ", missions=" + missions + ", crewAffiliation=" + crewAffiliation + ", version=" + version + "]";
	}


	
}
