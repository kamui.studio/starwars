package entities;

import javax.persistence.*;

@Entity
@Table(name="ships")
public class Ships {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;

	@Column(name="name", nullable=false)
	private String name;

	@Column(name="color", nullable=false)
	private String color;
	
	@Column(name="size", nullable=false)
	private String size;
	
	@Column(name="designer", nullable=false)
	private String designer;
	
	@Column(name="capacity", nullable=false)
	private int capacity;
	
	@Column(name="speed", nullable=false)
	private int speed;

	@OneToOne(mappedBy="ship")
	private Crews crew;

	@Version
	private int version;
	
	public Ships() {
		
	}

	public Ships(String name, String color, String size, String designer, int capacity, int speed) {
		super();
		this.name = name;
		this.color = color;
		this.size = size;
		this.designer = designer;
		this.capacity = capacity;
		this.speed = speed;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getDesigner() {
		return designer;
	}

	public void setDesigner(String designer) {
		this.designer = designer;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public Crews getCrew() {
		return crew;
	}

	public void setCrew(Crews crew) {
		this.crew = crew;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "Ships [name=" + name + ", color=" + color + ", size=" + size + ", designer=" + designer + ", capacity="
				+ capacity + ", speed=" + speed + "]";
	}
		
	
}
