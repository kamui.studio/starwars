package entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="crew_affiliations")
public class CrewsAffiliations {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;

	@Column(name="name", nullable=false)
	private String name;
	
	@Column(name="sigil", nullable=false)
	private String sigil;
	
	@OneToMany(mappedBy="crewAffiliation")
	private List<Crews> crews = new ArrayList<Crews>();

	@Version
	private int version;

	public CrewsAffiliations(String name, String sigil) {
		super();
		this.name = name;
		this.sigil = sigil;
	}

	public CrewsAffiliations() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getSigil() {
		return sigil;
	}

	public void setSigil(String sigil) {
		this.sigil = sigil;
	}


	public List<Crews> getCrews() {
		return crews;
	}

	public void setCrews(List<Crews> crews) {
		this.crews = crews;
	}
	
	public void addCrews(Crews c) {
		crews.add(c);
	}

	@Override
	public String toString() {
		return "CrewsAffiliations [name=" + name + ", sigil=" + sigil + "]";
	}
	
}
