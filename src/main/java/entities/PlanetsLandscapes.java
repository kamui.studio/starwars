package entities;

import java.util.*;

import javax.persistence.*;

@Entity
@Table(name="planets_landscapes")
public class PlanetsLandscapes {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;

	@Column(name="name", nullable=false)
	private String name;


	@ManyToMany(cascade=CascadeType.ALL)
	private List<Planets> planets = new ArrayList<Planets>();


	@Version
	private int version;


	public PlanetsLandscapes() {
	}
	
	public PlanetsLandscapes(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public List<Planets> getPlanets() {
		return planets;
	}

	public void setPlanets(List<Planets> planets) {
		this.planets = planets;
	}

	public void addPlanet(Planets p) {
		planets.add(p);
	}
	
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "PlanetsLandscapes [name=" + name + "]";
	}
	
}
