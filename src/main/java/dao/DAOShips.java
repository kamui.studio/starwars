package dao;

import java.util.*;
import javax.persistence.*;

import entities.Ships;
import utils.Context;

public class DAOShips implements DAO<Ships, Integer> {

	
	public void create(Ships a) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.persist(a);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

	public Ships findById(Integer uid) {
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

        Ships ship = EM.find(Ships.class, uid);

        EM.close();
        Context.destroy();
		return ship;
		
	}
	
	public List<Ships> findAll(){
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

        Query q = EM.createQuery("SELECT s FROM Ships s");
        List<Ships> ships = q.getResultList();

        EM.close();
        Context.destroy();
		return ships;
		
	}
	
	public void update(Ships a) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.merge(a);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}
	
	public void delete(Ships a) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.remove(EM.merge(a));

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

}