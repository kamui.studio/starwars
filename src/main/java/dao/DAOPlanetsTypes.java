package dao;

import java.util.*;
import javax.persistence.*;

import entities.PlanetsTypes;
import utils.Context;

public class DAOPlanetsTypes implements DAO<PlanetsTypes, Integer> {

	
	public void create(PlanetsTypes t) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.persist(t);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

	public PlanetsTypes findById(Integer uid) {
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

		PlanetsTypes type = EM.find(PlanetsTypes.class, uid);

        EM.close();
        Context.destroy();
		return type;
		
	}
	
	public List<PlanetsTypes> findAll(){
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

        Query q = EM.createQuery("SELECT pt FROM PlanetsTypes pt");
        List<PlanetsTypes> types = q.getResultList();

        EM.close();
        Context.destroy();
		return types;
		
	}
	
	public void update(PlanetsTypes t) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.merge(t);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}
	
	public void delete(PlanetsTypes t) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.remove(EM.merge(t));

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

}