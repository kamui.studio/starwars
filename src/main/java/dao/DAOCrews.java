package dao;

import java.util.*;
import javax.persistence.*;

import entities.Crews;
import utils.Context;

public class DAOCrews implements DAO<Crews, Integer> {

	
	public void create(Crews a) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.persist(a);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

	public Crews findById(Integer uid) {
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

        Crews crew = EM.find(Crews.class, uid);

        EM.close();
        Context.destroy();
		return crew;
		
	}
	
	public List<Crews> findAll(){
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

        Query q = EM.createQuery("SELECT a FROM Crews a");
        List<Crews> crews = q.getResultList();

        EM.close();
        Context.destroy();
		return crews;
		
	}
	
	public void update(Crews a) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.merge(a);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}
	
	public void delete(Crews a) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.remove(EM.merge(a));

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

}