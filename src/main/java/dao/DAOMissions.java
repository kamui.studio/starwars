package dao;

import java.util.*;
import javax.persistence.*;

import entities.Missions;
import utils.Context;

public class DAOMissions implements DAO<Missions, Integer> {

	
	public void create(Missions a) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.persist(a);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

	public Missions findById(Integer uid) {
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

        Missions mission = EM.find(Missions.class, uid);

        EM.close();
        Context.destroy();
		return mission;
		
	}
	
	public List<Missions> findAll(){
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

        Query q = EM.createQuery("SELECT m FROM Missions m");
        List<Missions> missions = q.getResultList();

        EM.close();
        Context.destroy();
		return missions;
		
	}
	
	public void update(Missions a) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.merge(a);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}
	
	public void delete(Missions a) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.remove(EM.merge(a));

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

}