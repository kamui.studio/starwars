package dao;

import java.util.*;
import javax.persistence.*;

import entities.CrewsAffiliations;
import utils.Context;

public class DAOCrewsAffiliations implements DAO<CrewsAffiliations, Integer> {

	
	public void create(CrewsAffiliations a) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.persist(a);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

	public CrewsAffiliations findById(Integer uid) {
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

		CrewsAffiliations affil = EM.find(CrewsAffiliations.class, uid);

        EM.close();
        Context.destroy();
		return affil;
		
	}
	
	public List<CrewsAffiliations> findAll(){
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

        Query q = EM.createQuery("SELECT ca FROM CrewsAffiliations ca");
        List<CrewsAffiliations> affils = q.getResultList();

        EM.close();
        Context.destroy();
		return affils;
		
	}
	
	public void update(CrewsAffiliations a) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.merge(a);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}
	
	public void delete(CrewsAffiliations a) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.remove(EM.merge(a));

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

}