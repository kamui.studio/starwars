package dao;

import java.util.*;
import javax.persistence.*;

import entities.Planets;
import utils.Context;

public class DAOPlanets implements DAO<Planets, Integer> {

	
	public void create(Planets a) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.persist(a);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

	public Planets findById(Integer uid) {
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

        Planets planet = EM.find(Planets.class, uid);

        EM.close();
        Context.destroy();
		return planet;
		
	}
	
	public List<Planets> findAll(){
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

        Query q = EM.createQuery("SELECT p FROM Planets p");
        List<Planets> planets = q.getResultList();

        EM.close();
        Context.destroy();
		return planets;
		
	}
	
	public void update(Planets a) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.merge(a);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}
	
	public void delete(Planets a) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.remove(EM.merge(a));

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

}