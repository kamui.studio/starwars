package dao;

import java.util.*;
import javax.persistence.*;

import entities.PlanetsSpecies;
import utils.Context;

public class DAOPlanetsSpecies implements DAO<PlanetsSpecies, Integer> {

	
	public void create(PlanetsSpecies s) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.persist(s);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

	public PlanetsSpecies findById(Integer uid) {
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

		PlanetsSpecies species = EM.find(PlanetsSpecies.class, uid);

        EM.close();
        Context.destroy();
		return species;
		
	}
	
	public List<PlanetsSpecies> findAll(){
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

        Query q = EM.createQuery("SELECT ps FROM PlanetsSpecies ps");
        List<PlanetsSpecies> species = q.getResultList();

        EM.close();
        Context.destroy();
		return species;
		
	}
	
	public void update(PlanetsSpecies s) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.merge(s);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}
	
	public void delete(PlanetsSpecies s) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.remove(EM.merge(s));

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

}