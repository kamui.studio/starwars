package dao;

import java.util.*;
import javax.persistence.*;

import entities.PlanetsLocalisations;
import utils.Context;

public class DAOPlanetsLocalisations implements DAO<PlanetsLocalisations, Integer> {

	
	public void create(PlanetsLocalisations l) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.persist(l);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

	public PlanetsLocalisations findById(Integer uid) {
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

		PlanetsLocalisations local = EM.find(PlanetsLocalisations.class, uid);

        EM.close();
        Context.destroy();
		return local;
		
	}
	
	public List<PlanetsLocalisations> findAll(){
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

        Query q = EM.createQuery("SELECT pl FROM PlanetsLocalisations pl");
        List<PlanetsLocalisations> locals = q.getResultList();

        EM.close();
        Context.destroy();
		return locals;
		
	}
	
	public void update(PlanetsLocalisations l) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.merge(l);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}
	
	public void delete(PlanetsLocalisations l) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.remove(EM.merge(l));

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

}