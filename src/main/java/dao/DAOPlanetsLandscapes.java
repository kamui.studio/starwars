package dao;

import java.util.*;
import javax.persistence.*;

import entities.PlanetsLandscapes;
import utils.Context;

public class DAOPlanetsLandscapes implements DAO<PlanetsLandscapes, Integer> {

	
	public void create(PlanetsLandscapes l) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.persist(l);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

	public PlanetsLandscapes findById(Integer uid) {
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

		PlanetsLandscapes land = EM.find(PlanetsLandscapes.class, uid);

        EM.close();
        Context.destroy();
		return land;
		
	}
	
	public List<PlanetsLandscapes> findAll(){
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

        Query q = EM.createQuery("SELECT pl FROM PlanetsLandscapes pl");
        List<PlanetsLandscapes> lands = q.getResultList();

        EM.close();
        Context.destroy();
		return lands;
		
	}
	
	public void update(PlanetsLandscapes l) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.merge(l);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}
	
	public void delete(PlanetsLandscapes l) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.remove(EM.merge(l));

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

}