<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Welcome to Hogwarts</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://www.marc-grondin.com/java/hogwarts/css/fonts.css">
	<link rel="stylesheet" href="https://www.marc-grondin.com/java/hogwarts/css/main.css">

</head>
<body>
	
	<c:forEach items="${missions}" var="m">
		name : ${m.name}<br>
		bounty : ${m.bounty}<br>
		level : ${m.level}/5<br>
		Location : ${m.locationPlanet.name}<br>
		Quest : ${m.questPlanet.name}
	</c:forEach>

	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
	<script src="https://www.marc-grondin.com/java/hogwarts/js/app.js"></script>

</body>
</html>